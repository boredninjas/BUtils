#include "InputMoveKeyLayer.h"
#include "Resources.h"

bool InputMoveKeyLayer::init() {
	if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0))) {
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();

	float yPlus = visibleSize.height * 0.05f;

	Button* btn_move_key_up = Button::create(joystick_up);
	this->setContentSize(Size(btn_move_key_up->getContentSize().width*3,btn_move_key_up->getContentSize().height*3));


	btn_move_key_up->setTouchEnabled(true);
	btn_move_key_up->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn_move_key_up->addTouchEventListener(
			CC_CALLBACK_2(InputMoveKeyLayer::onPressInputKeyButton, this));
	btn_move_key_up->setTag(MOVE_DIRECTION::UP);
	btn_move_key_up->setPosition(
			Vec2(this->getContentSize().width / 2,
					this->getContentSize().height - btn_move_key_up->getContentSize().height/2 + yPlus));
	this->addChild(btn_move_key_up);


	Button* btn_move_key_left = Button::create(joystick_left);
	btn_move_key_left->setTouchEnabled(true);
	btn_move_key_left->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn_move_key_left->addTouchEventListener(
			CC_CALLBACK_2(InputMoveKeyLayer::onPressInputKeyButton, this));
	btn_move_key_left->setTag(MOVE_DIRECTION::LEFT);
//	btn_move_key_left->setPosition(
//				Vec2(btn_move_key_left->getContentSize().width / 2, this->getContentSize().height / 2));
	btn_move_key_left->setPosition(Point(btn_move_key_up->getPosition().x - btn_move_key_up->getContentSize().width * 0.8f,
			this->getContentSize().height / 2.0f + yPlus));
		this->addChild(btn_move_key_left);


	Button* btn_move_key_right = Button::create(joystick_right);
	btn_move_key_right->setTouchEnabled(true);
	btn_move_key_right->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn_move_key_right->addTouchEventListener(
			CC_CALLBACK_2(InputMoveKeyLayer::onPressInputKeyButton, this));
	btn_move_key_right->setTag(MOVE_DIRECTION::RIGHT);
//	btn_move_key_right->setPosition(
//				Vec2(this->getContentSize().width - btn_move_key_right->getContentSize().width/2,
//						this->getContentSize().height / 2));
	btn_move_key_right->setPosition(Point(btn_move_key_up->getPosition().x + btn_move_key_up->getContentSize().width * 0.8f,
			this->getContentSize().height / 2.0f + yPlus));
		this->addChild(btn_move_key_right);


	Button* btn_move_key_down = Button::create(joystick_down);
	btn_move_key_down->setTouchEnabled(true);
	btn_move_key_down->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn_move_key_down->addTouchEventListener(
			CC_CALLBACK_2(InputMoveKeyLayer::onPressInputKeyButton, this));
	btn_move_key_down->setTag(MOVE_DIRECTION::DOWN);
	btn_move_key_down->setPosition(
			Vec2(this->getContentSize().width / 2, btn_move_key_down->getContentSize().height/2 + yPlus));
	this->addChild(btn_move_key_down);

	return true;
}
void InputMoveKeyLayer::onPressInputKeyButton(Ref* pSender, ui::Widget::TouchEventType eEventType)
{
	if (eEventType == ui::Widget::TouchEventType::BEGAN) {
		this ->schedule(schedule_selector(InputMoveKeyLayer::onKeyHold));

		auto button = dynamic_cast<Button*>(pSender);
		key_direction = (MOVE_DIRECTION)button->getTag();

	}else if (eEventType == ui::Widget::TouchEventType::ENDED || eEventType == ui::Widget::TouchEventType::CANCELED) {
		this ->unschedule(schedule_selector(InputMoveKeyLayer::onKeyHold));
	}
}
void InputMoveKeyLayer::onKeyHold(float delta)
{
	if (!_delegate) {
		CCLog("delegate null");
		return;
	}
	_delegate->responeWhenPressMoveKeyButton(key_direction);
}
