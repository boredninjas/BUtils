#ifndef __INPUT_MOVE_KEY_LAYER_H__
#define __INPUT_MOVE_KEY_LAYER_H__

#include "cocos2d.h"
#include "cocos/ui/CocosGUI.h"
#include "Constant.h"
using namespace cocos2d::ui;

USING_NS_CC;

class InputMoveKeyLayerDelegate{
public:
    virtual void responeWhenPressMoveKeyButton(MOVE_DIRECTION key_direction) = 0;
};

class InputMoveKeyLayer : public cocos2d::LayerColor
{
public:
	virtual bool init();
	CREATE_FUNC(InputMoveKeyLayer);
private:

	CC_SYNTHESIZE(InputMoveKeyLayerDelegate*,_delegate,Delegate);
    void onPressInputKeyButton(Ref* pSender, ui::Widget::TouchEventType eEventType);
    void onKeyHold(float delta);
    MOVE_DIRECTION key_direction;

    Size visibleSize;
};

#endif
