
#ifndef _BLeaderBoardContanst_h
#define _BLeaderBoardContanst_h

#include "cocos2d.h"
using namespace std;

//Global key
#define RANKINGWORLDMODE "isrankingworldmode"

//Define for BUserInfo
#define KEY_WORLD_NAME       "FB_Name"
#define KEY_WORLD_LOCATION   "FB_Location"
#define KEY_WORLD_ID         "FB_ID"
#define KEY_WORLD_SCORE      "Score"
#define KEY_WORLD_OJECTID    "objectId"

//Key for Parse
static const string appID="X-Parse-Application-Id: hYf8HYrrrvZTdUligYAdy5lKtqrzHg7i8XMR8Jig";
static const string restAPI="X-Parse-REST-API-Key: bVFb1yUe4n6NX4oSzW9UCy1FqwR1HsMjnA5EfGqs";
static const string classURL="https://api.parse.com/1/classes/SpringNinja";

#endif
