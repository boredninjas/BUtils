
#ifndef _BLeaderBoardResources_h
#define _BLeaderBoardResources_h


static const char s_ranking_title[] = "res/rankingscene/txt_learderboard.png";
static const char s_ranking_number[] = "res/rankingscene/number.png";
static const char s_ranking_board[] = "res/rankingscene/board.png";
static const char s_ranking_btnHome[] = "res/rankingscene/btn_home.png";
static const char s_ranking_btnPlay[] = "res/rankingscene/btn_play.png";
static const char s_ranking_background[] = "res/rankingscene/bg_ranking.png";
static const char s_ranking_loadingSprite[] = "res/rankingscene/loading.png";
static const char s_ranking_btnWorldMode[] = "res/rankingscene/btn_worldMode.png";
static const char s_ranking_btnWorldMode_Clicked[] = "res/rankingscene/btn_worldMode_CLicked.png";
static const char s_ranking_btnFriendMode[] = "res/rankingscene/btn_friendMode.png";
static const char s_ranking_btnFriendMode_CLicked[] = "res/rankingscene/btn_friendMode_Clicked.png";
static const char s_ranking_btnConnectFacebook_LogOut[] = "res/rankingscene/btn_connectFB_LogOut.png";
static const char s_ranking_btnConnectFacebook_LogIn[] = "res/rankingscene/btn_connectFB_LogIn.png";

static const char* s_leaderboard_images[] = {"res/rankingscene/txt_learderboard.png","res/rankingscene/number.png",
		"res/rankingscene/board.png","res/rankingscene/btn_home.png","res/rankingscene/btn_play.png",
		"res/rankingscene/bg_ranking.png","res/rankingscene/loading.png","res/rankingscene/btn_worldMode.png",
		"res/rankingscene/btn_worldMode_CLicked.png","res/rankingscene/btn_friendMode.png","res/rankingscene/btn_friendMode_Clicked.png",
		"res/rankingscene/btn_connectFB_LogOut.png","res/rankingscene/btn_connectFB_LogIn.png"};
//Font
static const char s_font_leaderboard[] = "fonts/GOODDC__.TTF";

#endif
