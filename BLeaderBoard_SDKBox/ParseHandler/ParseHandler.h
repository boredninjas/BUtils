
#ifndef __ParseHandler__
#define __ParseHandler__
#include <cocos/network/HttpClient.h>
#include <cocos/network/HttpResponse.h>
#include "BUserInfor.h"
#include "FacebookHandler.h"
#include <iostream>

USING_NS_CC;
using namespace cocos2d::network;
using namespace std;
enum TAG{
    TAG_FRIEND,TAG_WORLD
};




class ParseDelegate{
public:
    virtual void responseForQuerryTopFriend(vector<BUserInfor*> friendList){

    };
    virtual void responseForQuerryTopWorld(vector<BUserInfor*> worldList){

    };
    virtual void responseAfterCheckFacebookIDExistOnParse(){

      };
};


class ParseHandler: public FacebookDelegate
{
private:
    TAG tag;
public:
    ParseHandler();
    ~ParseHandler();
    CC_SYNTHESIZE(ParseDelegate*,_parseDelegate,ParseDelegate);
    CC_SYNTHESIZE(vector<BUserInfor*>,_worldList,WorldList);
    CC_SYNTHESIZE(vector<BUserInfor*>,_friendList,FriendList);

    static ParseHandler* getInstance();
    void checkFacebookIdExistOnParse();
    void checkFacebookIdExistOnParseCallBack(HttpClient* client,HttpResponse* response);
    //
    void saveFacebookIdOnParse(BUserInfor* user);
    void callBacksaveFacebookIdOnParse(HttpClient* client,HttpResponse* response);
    //
    void fetchBUserInforAt(char* querry);
    void callBackFetchBUserInforAt(HttpClient* client,HttpResponse* response);
    // get friend
    void fetchTopFriend();
    // get top world
    void fetchTopWorld();
    // submit score
    void submitScore(int score);



    virtual void responseWhenGetFriendsSuccessfully(string friendList);
    virtual void responseWhenGetMyInfoSuccessfully(BUserInfor* user);
};

#endif
