Hướng dẫn sử dụng
clone thư mục BLeaderBoard bỏ vô classes trong project
+ Đầu tiên là phải import facebook của SDKBox, test chạy thành công thì tiếp tục integrate BLeaderboard

+ Cấu hình:
mở file BLeaderBoardContanst.h lên chú ý ở đoạn:

#define KEY_WORD_NAME       "name"
#define KEY_WORD_EMAIL      "email"
#define KEY_WORD_ID         "facebookId"
#define KEY_WORD_SCORE      "score"
#define KEY_WORD_OJECTID    "objectId"

Vào Parse tạo ra class cho ứng dụng của mình và đặt các field trùng tên trong file BLeaderBoardContanst.h

Thay thế các key phù hợp với ứng dụng của bạn trong file BLeaderBoardContanst.h như ví dụ dưới đây:
// key for Parse
static const string appID="X-Parse-Application-Id: VkvGx7tugqBHJq63yg6341vQSjlXd2pix3P4bEEv";
static const string restAPI="X-Parse-REST-API-Key: AiaOq1EE30PYrIgheDdhnqez5tp7rYQpEApM1Q2L";
static const string classURL="https://api.parse.com/1/classes/LeaderBoard";
+ Hướng dẫn sử dụng code:
include 2 file FacebookHandler.h và ParseHandler.h
Cung cấp các hàm sau :
* Login Facebook
void FacebookHandler::getInstance->loginFacebook();
* Logout Facebook
void FacebookHandler::getInstance->loginFacebook();
------------- Các hàm sử dụng để hiển thị trên leaderBoard--------------
Trước hết bạn phải set delegate cho ParseHandler::getInstance trong Layer muốn vẽ
ví dụ có LeaderBoardLayer:
#include "BUserInfor.h"
class LeaderBoardLayer:public ParseDelegate{
public:
// override hai hàm này, hai hàm này là response của hàm lấy danh sách bạn bè và danh sách người chơi top
virtual void responseForQuerryMyFriend(vector<BUserInfor*> friendList);
virtual void responseForQuerryTopWord(vector<BUserInfor*> worldList);
}
Trong class phải set delegate:
ParseHandler::getInstance->setDelegate(this);
* Lấy danh sách bạn bè trong facebook và hiển thị trên Leaderboard:
void FacebookHandler::getInstance->getFriends();
* Lấy danh sách người chơi top:
void ParseHandler::getInstance()->fetchTopWord();
* Submit score lên Parse:
void ParseHandler::getInstance()->submitScore(int hightScore);















