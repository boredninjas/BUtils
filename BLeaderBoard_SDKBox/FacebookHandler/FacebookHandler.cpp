//
//  FacebookHandler.cpp
//  SonTinhThuyTinh
//
//  Created by cao Ky Han on 6/18/15.
//
//

#include "FacebookHandler.h"
#include "BLeaderBoardContanst.h"
#include "BUserInfor.h"
#include "ParseHandler.h"
FacebookHandler::FacebookHandler()
{

}

FacebookHandler::~FacebookHandler()
{
    
}

FacebookHandler* FacebookHandler::getInstance()
{
    static FacebookHandler* instance;
    if (instance==nullptr) {
        instance=new FacebookHandler();
        sdkbox::PluginFacebook::setListener(instance);
    }
    return instance;
}
string FacebookHandler::getUserFacebookID()
{
	return sdkbox::PluginFacebook::getUserID();
}
void FacebookHandler::getAllFriendsID()
{
	sdkbox::PluginFacebook::FBAPIParam params;
	sdkbox::PluginFacebook::fetchFriends();
}

void FacebookHandler::loginFacebook()
{
	sdkbox::PluginFacebook::requestReadPermissions({"public_profile", "user_friends"}); //Including login

}
void FacebookHandler::logoutFacebook()
{
	sdkbox::PluginFacebook::logout();
	if(_facebookConnectDelegate != nullptr)
		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
bool FacebookHandler::isFacebookLoggedIn()
{
	if (sdkbox::PluginFacebook::isLoggedIn())
		return true;
	else
		return false;
}
void FacebookHandler::getMyProfile()
{
	sdkbox::PluginFacebook::FBAPIParam params;
	params["fields"] = "name,id,locale";
	sdkbox::PluginFacebook::api("/me", "GET", params, "/me");
}


/*********************
 * Facebook callbacks
 *********************/
void FacebookHandler::onLogin(bool isLogin, const std::string& error)
{
	if(_facebookConnectDelegate != nullptr && isLogin)
		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
void FacebookHandler::onAPI(const std::string& tag, const std::string& jsonData)
{
    CCLog("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());

   if(tag == "/me")
   {
	  BUserInfor* user=BUserInfor::parseUserFrom(jsonData);
	  user->setScore(0);

	  if(_facebookDelegate != nullptr)
		_facebookDelegate->responseWhenGetMyInfoSuccessfully(user);
   }
}
void FacebookHandler::onSharedSuccess(const std::string& message)
{
    CCLog("##FB onSharedSuccess:%s", message.c_str());

    MessageBox(message.c_str(), "share success");
}
void FacebookHandler::onSharedFailed(const std::string& message)
{
    CCLog("##FB onSharedFailed:%s", message.c_str());

    MessageBox(message.c_str(), "share failed");
}
void FacebookHandler::onSharedCancel()
{
    CCLog("##FB onSharedCancel");
}
void FacebookHandler::onPermission(bool isLogin, const std::string& error)
{
    if(_facebookConnectDelegate != nullptr && isLogin)
    		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
void FacebookHandler::onFetchFriends(bool ok, const std::string& msg)
{
    CCLog("##FB %s: %d = %s", __FUNCTION__, ok, msg.data());

    string friendList="";
    const std::vector<sdkbox::FBGraphUser>& friends = sdkbox::PluginFacebook::getFriends();
    for (int i = 0; i < friends.size(); i++)
	{
		const sdkbox::FBGraphUser& user = friends.at(i);
		friendList=friendList+"\""+user.uid.data()+"\",";
	}
    
    if(_facebookDelegate != nullptr)
    		_facebookDelegate->responseWhenGetFriendsSuccessfully(friendList);
}
