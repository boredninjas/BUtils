#include "HelloWorldScene.h"
#include "BScrollView.h"
USING_NS_CC;

Scene* HelloWorld::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init() {
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(255,255,255,255))) {
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create("CloseNormal.png",
			"CloseSelected.png",
			CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	closeItem->setPosition(
			Vec2(
					origin.x + visibleSize.width
							- closeItem->getContentSize().width / 2,
					origin.y + closeItem->getContentSize().height / 2));

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	addScrollView();

	return true;
}
void HelloWorld::addScrollView() {
	//Scrollview configuration
	int numberOfItems = 20;
	float itemMargin = 125;
	Size scrollFrameSize = Size(500, 650);

	//Create scrollview
	BScrollView* scrollview = BScrollView::createVertical(numberOfItems,itemMargin,scrollFrameSize);
	scrollview->setPosition(
			Vec2(visibleSize.width / 2,visibleSize.height / 2));
	scrollview->setBackGroundColorType(Layout::BackGroundColorType::SOLID); //Background
	scrollview->setBackGroundColor(Color3B(200, 200, 200)); //Background
	this->addChild(scrollview);

	//Add sth to scroll view
	float positionX = scrollview->leftPosition;
	float positionY = scrollview->topPosition;
	for (int i = 0; i < numberOfItems; i++) {
		//Item background button
		Sprite* itemSprite = Sprite::create("coupon.png");
		itemSprite->setAnchorPoint(Point(0.5f, 0.5f));
		itemSprite->setPosition(Vec2(positionX + scrollFrameSize.width / 2 - itemMargin/2,positionY));
		scrollview->addChild(itemSprite);

		positionY -= itemMargin;
	}
}

void HelloWorld::menuCloseCallback(Ref* pSender) {

}
